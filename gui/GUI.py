# -*- coding: utf-8 -*-

"""
Created for WormbaseFetcher
@date:      08/02/2016
@auhor:     Jehudith Dorfman
@version:   1.0

Made with PyCharm Community Edition
"""
from tkinter import *
from tkinter import messagebox, filedialog

from controller.DataController import DataController


class GUI(Tk):
    """
    this is the graphical user interface for the wormbase fetcher
    """

    def __init__(self):
        super(GUI, self).__init__()
        self.initialize()

    def initialize(self):
        """
        First frame settings
        """
        # amount of pixel
        self.width = 800
        self.height = 600

        # this sets the main window width and height
        self.geometry('{}x{}'.format(self.width, self.height))
        self.title("Wormbase Fetcher")

        # No stretching possible
        self.resizable(False, False)

        # main frame for easy management of window
        self.main_frame = None

        # current gene id page
        self.page = 0

        # remembered genes
        self.remembered_genes = []

        # no data-logic in gui, data controller fetches data!
        self.controller = DataController()

        # build initial screen, possible to give gene id list
        self.init_main_frame()
        self.show_init_screen()

    def init_main_frame(self):
        """
        The main frame is build on top of the initialize frame
        It is easy to clear the window
        """
        # Setting up frame on root window
        self.main_frame = Frame(self, height=self.height, width=self.width)
        self.main_frame.pack(fill=BOTH, expand=True)

        # Header frame
        self.header = Frame(self.main_frame, height=100, width=self.width)
        self.header.pack(side=TOP, fill=BOTH)

        # Body frame
        self.body = Frame(self.main_frame, height=300, width=self.width)
        self.body.pack(fill=BOTH, expand=True)

        # Footer frame
        self.footer = Frame(self.main_frame, height=100, width=self.width)
        self.footer.pack(side=BOTTOM, fill=BOTH)

    def show_init_screen(self):

        # Label
        label = Label(self.header, height=1, width=1, text="Enter Wormbase ID list:")
        label.pack(fill=X, expand=True)

        # Text
        self.txt = Text(self.body, height=1, width=1, borderwidth=2, relief="sunken")
        self.txt.config(font=("consolas", 12), undo=True, wrap='word')
        self.txt.pack(side=LEFT, fill=BOTH, expand=True)

        # Scrollbar
        self.attach_scrollbar(self.txt, self.body).pack(side=RIGHT, fill=Y)

        # Buttons
        button = Button(self.footer, text="Next", command=self.get_gene_id_list)
        button.pack(fill=Y, expand=True)

    def show_gene_info(self, gene_id):
        overview = self.controller.get_overview_data(gene_id)
        interaction_text = self.controller.get_interaction_text(gene_id)

        # Gene ID
        gene_id_label = Text(self.header, height=1, width=1, borderwidth=0)
        gene_id_label.insert("1.0", gene_id)
        gene_id_label.config(state="disabled")
        gene_id_label.pack(side=LEFT, fill=BOTH, expand=True)

        # Remember Checkbox
        self.remembered = IntVar()
        chkbox = Checkbutton(self.header, text="Remember gene", variable=self.remembered, command=self.remember_gene)
        chkbox.pack(side=RIGHT, fill=Y)
        if gene_id in self.remembered_genes:
            chkbox.select()

        # Concise Desription Top
        consise_frame = Frame(self.body, height=75, width=self.width)
        consise_frame.pack(side=TOP, fill=BOTH, expand=True)

        concise = Text(consise_frame, height=1, width=1, borderwidth=3, relief="sunken")
        concise.config(font=("consolas", 12), undo=True, wrap='word', state="disabled")
        concise.pack(side=LEFT, fill=BOTH, expand=True)
        self.attach_scrollbar(concise, consise_frame).pack(side=RIGHT, fill=Y)
        self.insert_text(concise, overview, "concise_description")

        interactions_frame = Frame(self.body, height=75, width=self.width)
        interactions_frame.pack(fill=BOTH, expand=True)

        interactions = Text(interactions_frame, height=1, width=1, borderwidth=3, relief="sunken")
        interactions.config(font=("consolas", 12), undo=True, wrap='word', state="disabled")
        interactions.pack(side=LEFT, fill=BOTH, expand=True)
        self.attach_scrollbar(interactions, interactions_frame).pack(side=RIGHT, fill=Y)
        self.insert_plain_text(interactions, interaction_text)

        subbody_frame = Frame(self.body, height=300/2, width=self.width)
        subbody_frame.pack(side=BOTTOM, fill=BOTH, expand=True)

        provisional_frame = Frame(subbody_frame, height=300/2, width=self.width/2)
        provisional_frame.pack(side=LEFT, fill=BOTH, expand=True)

        # Provisional Description Left Bottom
        provisional = Text(provisional_frame, height=1, width=1, borderwidth=1, relief="sunken")
        provisional.config(font=("consolas", 12), undo=True, wrap='word', state="disabled")
        provisional.pack(side=LEFT, fill=BOTH, expand=True)
        self.attach_scrollbar(provisional, provisional_frame).pack(side=RIGHT, fill=Y)
        self.insert_text(provisional, overview, "provisional_description")

        # Human diseases Description
        hd_frame = Frame(subbody_frame, height=300/2, width=self.width/2)
        hd_frame.pack(side=RIGHT, fill=BOTH, expand=True)

        hd = Text(hd_frame, height=1, width=1, borderwidth=1, relief="sunken")
        hd.config(font=("consolas", 12), undo=True, wrap='word', state="disabled")
        hd.pack(side=LEFT, fill=BOTH, expand=True)
        self.attach_scrollbar(hd, hd_frame).pack(side=RIGHT, fill=Y)
        self.insert_text(hd, overview, "human_disease_relevance")

        # Buttons
        insert = Button(self.footer, text="Insert new list", command=self.fetch_new)
        insert.pack(side=LEFT, fill=Y)
        export = Button(self.footer, text="Export genes", command=self.export_remembered_genes)
        export.pack(side=LEFT, fill=Y)
        next_gen = Button(self.footer, text="next", command=self.next)
        next_gen.pack(side=RIGHT, fill=Y)
        previous_gen = Button(self.footer, text="previous", command=self.previous)
        previous_gen.pack(side=RIGHT, fill=Y)

        # Check if error
        if overview is not None and not overview["success"]:
            messagebox.showerror("Error", "Fetching of " + gene_id + " failed!\nPlease lookup manually")

    def get_gene_id_list(self):
        """
        The user inserts a gene list of choice
        """
        input = self.txt.get("1.0", END)
        input = input.strip()

        # Check if gene_id's are correct
        if input == "" or input[0:2] != "WB":
            messagebox.showerror("Error", "Please insert gene id list first (Seperated by enters)")
            return

        self.gene_id_list = input.split("\n")
        print(self.gene_id_list)

        self.next()

    def fetch_new(self):
        """
        When the user decides to  insert a new list
        """
        choice = messagebox.askyesno("Question", "Are you sure you wish to insert new list?\nCurrent data will be lost")
        if not choice:
            return

        # Show init screen
        self.page = 0
        self.remembered_genes = []
        self.clear()
        self.show_init_screen()

    def clear(self):
        """
        Clears the old frame and build a new for user again
        """
        self.main_frame.pack_forget()
        self.main_frame.destroy()
        self.init_main_frame()

    def insert_text(self, obj, text_dict, key):
        """
        The return info from wormbase will be inserted in the text box
        :param obj: words are inserted in window
        :param text_dict: info from the request
        :param key: specific dict key to retrieve the value (text) in the frame
        """
        obj.config(font=("consolas", 12), undo=True, wrap='word', state="normal")
        if text_dict is None:
            obj.insert("1.0", "Could not find data")
            return

        if key not in text_dict.keys():
            obj.insert("1.0", "Could not find data")
            return
        if isinstance(text_dict[key], str):
            obj.insert("1.0", text_dict[key])
        else:
            for item in text_dict[key]:
                obj.insert("1.0", item["text"] + "\n")
        obj.config(font=("consolas", 12), undo=True, wrap='word', state="disabled")

    def insert_plain_text(self, obj, text):
        """
        The return info from wormbase will be inserted in the text box
        """
        obj.config(font=("consolas", 12), undo=True, wrap='word', state="normal")
        if text is None:
            obj.insert("1.0", "Could not find data")
            return

        obj.insert("1.0", text)
        obj.config(font=("consolas", 12), undo=True, wrap='word', state="disabled")

    def attach_scrollbar(self, obj, parent):
        # Scrollbar
        scrollb = Scrollbar(parent, command=obj.yview)
        obj['yscrollcommand'] = scrollb.set
        return scrollb

    def next(self):
        """
        the gene list starts with index 0 and the page with index 1
        """
        if self.page + 1 > len(self.gene_id_list):
            messagebox.showinfo("Info", "No next page available")
            return
        self.page += 1

        # Show the gene page for the current page
        self.show_gene_page()

    def previous(self):
        if self.page - 1 < 1 or len(self.gene_id_list) == 0:
            messagebox.showinfo("Info", "No previous page available")
            return
        self.page -= 1

        # Show the gene page for the current page
        self.show_gene_page()

    def show_gene_page(self):
        # Return default window state
        self.clear()

        # Show gene page
        self.show_gene_info(self.gene_id_list[self.page-1])

    def remember_gene(self):
        gene = self.gene_id_list[self.page-1]
        if gene in self.remembered_genes:
            # Delete from list
            self.remembered_genes.remove(gene)
        else:
            # Add to list
            self.remembered_genes.append(self.gene_id_list[self.page-1])

    def export_remembered_genes(self):
        if len(self.remembered_genes) == 0:
            messagebox.showinfo("Info", "No genes selected for export")
            return

        # Open filepath
        file_path = filedialog.asksaveasfilename(initialdir="C:\\", defaultextension=".csv")
        if file_path == "":
            return

        # give the gene id list to the datacontroller function convert
        success = self.controller.write_ids_to_csv(file_path, self.remembered_genes)
        if not success:
            messagebox.showerror("Error", "Export failed")
