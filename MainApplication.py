# -*- coding: utf-8 -*-

"""
Created for WormbaseFetcher
@date:      24/01/2016
@auhor:     Jehudith Dorfman
@version:   1.0

Made with PyCharm Community Edition
"""
from gui.GUI import GUI
from handlers.requesthandlers.WormbaseRequestHandler import WormbaseRequestHandler


class MainApplication(object):
    """
    This file runs the WormbaseFetcher after receiving a list of genes
    help
    command input
    while om id te voeren, true

    """

    def __init__(self):
        super(MainApplication, self).__init__()

    def run(self):
        """
        This function runs the whole program (WormbaseFetcher), the gui is the interface .
        """
        # this is the interface pop-up
        gui = GUI()

        # while loop build in the gui library; instead of exit-click x on the interface
        gui.mainloop()

















