# -*- coding: utf-8 -*-

"""
Created for WormbaseFetcher
@date:      24/01/2016
@auhor:     Jehudith Dorfman
@version:   1.0

Made with PyCharm Community Edition
"""
from MainApplication import MainApplication
from controller.DataController import DataController
from gui.GUI import GUI
from handlers.requesthandlers.WormbaseRequestHandler import WormbaseRequestHandler

if __name__ == "__main__":

    app = MainApplication()
    #test = DataController()
    #print(test.get_interaction_text('WBGene00000231'))
    app.run()

