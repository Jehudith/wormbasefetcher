# -*- coding: utf-8 -*-

"""
Created for Fetcher
@date:      09/02/2016
@auhor:     Jehudith Dorfman
@version:   1.0

Made with PyCharm Community Edition
"""
from handlers.dicthandler.DictCompressor import DictCompressor
from handlers.requesthandlers.WormbaseRequestHandler import WormbaseRequestHandler
from handlers.filehandlers.CSVWriteHandler import CSVWriteHandler


class DataController(object):
    """
    Checkes the incoming gene list
    """

    def __init__(self):
        super(DataController, self).__init__()
        self.wb_handler = WormbaseRequestHandler()
        self.compressor = DictCompressor()
        self.csvhandler = CSVWriteHandler()

    def get_overview_data(self, gene_id):
        """
        Receives the genes inserted by the user in the gui interface
        :param gene_id:
        :return: None when no overview is present or when overview is present return it
        """
        overview = self.wb_handler.get_overview(gene_id)
        if overview is None:
            return None

        # Catch exceptions when compressing fails
        try:
            overview = self.compressor.compress_overview_dict(overview)
            overview["success"] = True
        except:
            # Compression failed
            overview = {"success": False}

        return overview

    def convert_list_to_dict(self, gene_id_list):
        """
        This function receives the exported gene list from the gui
        :param gene_id_list:
        :return: a list with dict items
        """

        # empty list for the dict items [{gene-id,wbxxx}]
        csv_list = []

        # loop over each id in the list
        for wb in gene_id_list:

            # make in the loop a new dict
            dict_csv = {"gene_id": wb}
            csv_list.append(dict_csv)
        return csv_list

    def write_ids_to_csv(self, filepath, gene_id_list):
        """
        this function is responsible to write the id in the csv
        It controlls opening a csv writing and closes
        """
        csv_list = self.convert_list_to_dict(gene_id_list)
        return self.csvhandler.write_to_file(filepath, csv_list)

    def get_interaction_text(self, gene_id):
        interaction = self.wb_handler.get_interaction(gene_id)

        if interaction is None:
            return None

        # Catch exceptions when compressing fails
        try:
            interaction_list = self.compressor.compress_interaction(interaction)
        except:
            # Compression failed
            print("There is no interaction list in get_interaction_text")
            return None

        if len(interaction_list) == 0:
            return "NA"

        interaction_string = ""
        for interact_dict in interaction_list:
            type_act = interact_dict['type']
            interaction_detail_list = interact_dict['interactions']

            # take the interact genes
            for gene in interaction_detail_list:
                label = gene['label']
                interaction_string += "{0:30} | {1}\n".format(label, type_act)
                interaction_string += ("-" * 80) + "\n"

        return interaction_string
