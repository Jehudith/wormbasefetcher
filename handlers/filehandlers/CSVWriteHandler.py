# -*- coding: utf-8 -*-

"""
Created for WormbaseFetcher
@date:      08/02/2016
@auhor:     Jehudith Dorfman
@version:   1.0

Made with PyCharm Community Edition
"""
import csv
from handlers.filehandlers.BaseWriteHandler import BaseWriteHandler

class CSVWriteHandler(BaseWriteHandler):
    """
    Comment for CSVWriteHandler
    """

    def __init__(self):
        super(CSVWriteHandler, self).__init__()

    # Override
    def write_to_file(self, file_name, csv_row_list, fieldnames=[]):
        """
        This function write the input in an excelworksheet
        """
        if not self.open_file(file_name):
            return False

        if self.file_handler is None:
            return False

        if len(csv_row_list) == 0:
            return False

        # If no fieldnames specified, use keys of dict
        if len(fieldnames) == 0:
            fieldnames = csv_row_list[0].keys()

        try:
            # write the dict data into a csv file
            writer = csv.DictWriter(self.file_handler,
                                    fieldnames=fieldnames,
                                    delimiter=';',
                                    lineterminator='\n',
                                    dialect="excel")
            writer.writeheader()

            # Looping rows to write
            for row in csv_row_list:
                # Check if equal to fieldnames
                if row.keys() != fieldnames:
                    return False
                writer.writerow(row)
            self.close()
        except:
            print("A error occured while writing")
            self.close()
            return False

        # Success
        return True
