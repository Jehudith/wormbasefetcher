# -*- coding: utf-8 -*-

"""
Created for WormbaseFetcher
@date:      04/02/2016
@auhor:     Jehudith Dorfman
@version:   1.0

Made with PyCharm Community Edition
"""
import os

class BaseWriteHandler(object):
    """
    Base class for output in excel
    """

    def __init__(self):
        super(BaseWriteHandler, self).__init__()
        self.file_handler = None

    def write_to_file(self, file_name, msg):
        """
        This function write the input in a file
        """
        if self.path_exists(file_name):
            return None

        if not self.open_file(file_name):
            return None

        if self.file_handler is None:
            return None
        try:
           self.file_handler.write(msg)

        except:
            print("A error occured while writing")

            self.close()

    def path_exists(self, file_name):
        """
        This function checks if path exists
        :param file_name:
        :return: False if path does not exists
        """
        if not os.path.exists(file_name):
            return False
        else:
            print("file already exists")
            return True

    def open_file(self, file_name):
        """
        This functions tries to open a file with the handler.
        :param file_name:
        :return: True
        """
        try:
            self.file_handler = open(file_name, "w", newline="")
            return True
        except:
            print("error occured in opening file")
            return False

    def close(self):
        try:
            self.file_handler.close()
            self.file_handler = None
        except:
            print("Could not close file handler")

