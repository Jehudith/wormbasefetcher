
# -*- coding: utf-8 -*-

"""
Created for WormbaseFetcher
@date:      24/01/2016
@auhor:     Jehudith Dorfman
@version:   1.0

Made with PyCharm Community Edition
"""
import requests
from config import *


class BaseRequestHandler(object):
    """

    request client,get, request sturen naar een url
    """

    def __init__(self):
        super(BaseRequestHandler, self).__init__()

    def get(self, url):
        """
        make request, and get response r
        Get the url (from wormbaserequesthandler), try decode into json.
         """
        try:
            #retrieve data in json format
            headers = {"content-type": "application/json",
                       "User-Agent": "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) "
                                     "Chrome/41.0.2228.0 Safari/537.36"}

            r = requests.get(url, headers=headers)
        except:
            if DEBUG_MODE:
                #request failed
                print("BaseRequestHandler: get(request) failed")
            return None
        if not self.status_code_ok(r):
            return None
        return self.get_json(r)

    def status_code_ok(self, r):
        """
        the GET Request will response with a status number (standard response for successful HTTP requests)
        (the number is an entity corresponding to the request resource)
        :param r: json response (format)
        :return: True
        """
        #successful HTTP request
        if not r.status_code == 200:
            if DEBUG_MODE:
                #gives statusF
                print("BaseRequestHandler: status_code not 200 but:", r.status_code)
                print("BaseRequestHandler: Request returned:", r.text)
            return False
        return True

    def get_json(self, r):
        """
        string decode to json format
        :param r: string which contains a dict format
        :return: decode dict
        """
        try:
            #from string to python; decode(string to object)
            decode_dict = r.json()

        except:
            if DEBUG_MODE:
                print("BaseRequestHandler: decode json fail")
            return None
        return decode_dict






