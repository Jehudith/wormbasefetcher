# -*- coding: utf-8 -*-

"""
Created for WormbaseFetcher
@date:      24/01/2016
@auhor:     Jehudith Dorfman
@version:   1.0

Made with PyCharm Community Edition
"""
from handlers.requesthandlers.BaseRequestHandler import BaseRequestHandler
from config import *


class WormbaseRequestHandler(BaseRequestHandler):
    """
    contains calls to the wormbase apis
    """

    def __init__(self):
        super(WormbaseRequestHandler, self).__init__()

    def get_overview(self, gene_id):
        """
        overview information about genes
        :param gene_id: list type
        :return: self.get(url)
        """
        url = WB_API_BASE_URL + "/widget/gene/" + gene_id + "/overview"
        if DEBUG_MODE:
            print("retrieving overview with", url)
        return self.get(url)

    def get_human_disease(self, gene_id):

        url = WB_API_GENE_URL + "/" + gene_id + "/human_diseases"
        if DEBUG_MODE:
            print("retrieving human disease with", url)
        return self.get(url)

    def get_homology(self, gene_id):

        url = WB_API_GENE_URL + "/" + gene_id + "/homology"
        if DEBUG_MODE:
            print("retrieving homology", url)
        return self.get(url)

    def get_gene_ontology(self, gene_id):

        url = WB_API_GENE_URL + "/" + gene_id + "/gene_ontology"
        if DEBUG_MODE:
            print("retrieving gene ontology", url)
        return self.get(url)

    def get_interaction(self, gene_id):

        url = WB_API_GENE_URL + "/" + gene_id + "/interactions"
        if DEBUG_MODE:
            print("retrieving interactions", url)
            print(url)
        return self.get(url)




