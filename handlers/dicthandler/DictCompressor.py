# -*- coding: utf-8 -*-

"""
Created for Fetcher
@date:      09/02/2016
@auhor:     Jehudith Dorfman
@version:   1.0

Made with PyCharm Community Edition
"""
from handlers.requesthandlers.WormbaseRequestHandler import WormbaseRequestHandler

class DictCompressor(object):
    """
    Comment for DictCompressor
    """

    def __init__(self):
        super(DictCompressor, self).__init__()

    def compress_overview_dict(self, hugedict):
        """
        This function receives the (decoded json string)= dict
        :param hugedict:
        :return: specific descriptions of the gene are added in a new dict
        """

        # create empty dict to append everything needed
        compact_dict = {}

        # get everything about the concise description from the huge dict
        compact_dict["concise_description"] = hugedict["fields"]["concise_description"]["data"]["text"]
        #print(compact_dict)

        # if the concise description is not available give that back NA
        if compact_dict["concise_description"] is None:
            compact_dict["concise_description"] = "NA"

        # this is the list which contains the complete data of human disease relevance (list with dict)
        human_disease_relevance_list = hugedict["fields"]["human_disease_relevance"]["data"]
        #print(human_disease_relevance_list)

        # This dict contains the text of human disease relevance
        compact_dict = self.get_text_dict(human_disease_relevance_list, "human_disease_relevance", compact_dict)
        #print(compact_dict)

        # get everything about the provisional data from the huge dict
        provisional_data = hugedict["fields"]["structured_description"]["data"]
        #print(provisional_data)

        # provisional data can be none, if so write NA
        if provisional_data is None or "Provisional_description" not in provisional_data:
            compact_dict["provisional_description"] = "NA"

        # this condition gives the text about the provisional description
        else:
            provisional_data_list = provisional_data["Provisional_description"]
            #print(provisional_data_list)
            compact_dict = self.get_text_dict(provisional_data_list, "provisional_description", compact_dict)

        #print(compact_dict)
        return compact_dict

    def compress_interaction(self, hugedict):
        """

        :param hugedict:
        :return:
        """
        interaction_list = []
        print(hugedict)
        edges = hugedict["interactions"]["interactions"]["data"]["edges"]
        if edges is None:
            return []

        for dictunit in edges:
            # the genes who interact
            interact_genes = dictunit["interactions"]
            type_interaction = dictunit["type"]

            genes = {"interactions": interact_genes}
            genes["type"] = type_interaction

            interaction_list.append(genes)
        return interaction_list


    def compress_homology_dict(self, hugedict):
        """
        This functions also receives a huge (decoded json) dict
        :param hugedict:
        :return: in a new dict the h. sapiens homology
        """
        #let-706
        #hugedict =

        #hugedict =

        #hugedict = json.loads(hugedict)

        # empty compact dict
        compact_dict = {}

        # this compact dict works if there are homologues
        compact_dict["homology"] = hugedict["homology"]["nematode_orthologs"]["data"]
        print(compact_dict)

        # get everything from the dict
        #if compact_dict["homology"] is None:
            #compact_dict["homology"] = "NA"

        #print(compact_dict)

        for allhomology in compact_dict:
            homology_list = compact_dict[allhomology]
            #print(homology_list)
            for dictiterable in homology_list:
                compact_list = dictiterable["ortholog"]
                #print(compact_list)
                keys = compact_list.keys()
                values = compact_list.values()
                #print(keys)
                #print(values)



                #geef me alle dicts in deze lijst die de taxonomy value h.sapiens hebben

                for specific_dict in compact_list:
                    #print(specific_dict)

                    values = compact_list[specific_dict]
                    #print(values)

                    list_h_sapiens_homologues = []
                    try:
                        if values == "h_sapiens":
                            #print(values)
                            #I want the dicts which have the h_sapiens value
                            list_h_sapiens_homologues.append(compact_list)

                            #print(list_h_sapiens_homologues)

                    except:
                        print("no human orthologes where found")

    def gene_ontology(self):
        pass

    def get_text_dict(self, list_to_process, key, compact_dict):
        """
        This function is responsible to retrieve the text from the list and add it to the final dict
        :param list_to_process:
        :param key:
        :param compact_dict:
        :return:compact dict which contains the desired items (dict)
        """
        # If incoming data list is empty return it empty list
        if list_to_process is None:
            list_to_process = []

        # check all dicts in list, with index from every dict
        for index, item in enumerate(list_to_process):
            # create dict
            new_dict = {}

            # if no text was founded just continued
            if not "text" in item: continue

            # left: is the new key with right: the value from the list
            new_dict["text"] = item["text"]
            #print(new_dict)

            # The complete index from the list is overwrited with the new dict
            list_to_process[index] = new_dict

        # if there was no list or none, give NA back
        if not list_to_process:
            compact_dict[key] = "NA"
        else:
            # put the new list (value) with the related key, the key was given in the function as parameter
            compact_dict[key] = list_to_process
            #print(key)

        return compact_dict

if __name__ == "__main__":
    dcompres = DictCompressor()
    dcompres.compress_overview_dict(None)
    #dcompres.compress_homology_dict(None)
    #dcompres.gene_ontology()

