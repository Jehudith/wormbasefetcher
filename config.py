# -*- coding: utf-8 -*-

"""
Created for WormbaseFetcher
@date:      24/01/2016
@auhor:     Jehudith Dorfman
@version:   1.0

Made with PyCharm Community Edition
"""

"""
The config file contains global variables.
The base url of wormbase has a variable
This program is interested in gene_id therefore, there is a general gene_url
This file can be used in every class
"""

DEBUG_MODE = True

# API
WB_API_BASE_URL = "http://api.wormbase.org/rest"
WB_API_GENE_URL = WB_API_BASE_URL + "/field/gene"
